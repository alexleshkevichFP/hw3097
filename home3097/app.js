const express = require('express')
const app = express()

const {query, validationResult} = require('express-validator');

app.get('/', function (req, res) {
    res.write('<html>');
    res.write('<head><title>Homework 3097</title></head>');
    res.write('<body>');
    res.write('<form method="get" action="user"><input name="email" type="text" placeholder="email"><br><input name="password" type="text" placeholder="password"><button>Send</button></form>')
    res.write('</body>');
    res.write('</html>');
    res.end();
});

app.get('/user', [
    // username must be an email
    query('email').isEmail(),
    // password must be at least 5 chars long
    query('password').isLength({min: 5})
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors)
        const emailControl = '<input name="email" value=' + JSON.stringify(escapeHTML(req.query.email)) + ' type="text" placeholder="email"><br>';
        const passwordControl = '<input name="password" value=' + JSON.stringify(escapeHTML(req.query.password)) + ' type="text" placeholder="password"><br>';

        res.write('<html>');
        res.write('<head><title>Homework 3097</title></head>');
        res.write('<body>');
        for (let err of errors.errors) {
            res.write(`Input: ${err.param} INVALID!<br>`)
        }
        res.write('<form method="get" action="user">')
        res.write(emailControl);
        res.write(passwordControl);
        res.write('<button>Send</button>')
        res.write('</form>')
        res.write('</body>');
        res.write('</html>');
        return res.end();
    }

    res.write('<html>');
    res.write('<head><title>Homework 3097</title></head>');
    res.write('<body>');
    res.write(`VALID: ${req.query.email}!<br>`);
    res.write(`VALID: ${req.query.password}!<br>`);
    res.write('<form method="get" action="user"><input name="email" type="text" placeholder="email"><br><input name="password" type="text" placeholder="password"><button>Send</button></form>')
    res.write('</body>');
    res.write('</html>');
    res.end();

    console.log(req.query)
})

app.listen(7480)

function escapeHTML(text) {
    if ( !text )
        return text;
    text=text.toString()
        .split("&").join("&amp;")
        .split("<").join("&lt;")
        .split(">").join("&gt;")
        .split('"').join("&quot;")
        .split("'").join("&#039;");
    return text;
}
